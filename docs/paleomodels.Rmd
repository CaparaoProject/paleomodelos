---
title: "Past isolation of the Brazilian high-altitude grasslands through paleodistribution models"
author: - Andrea Sánchez-Tapia \* ^*1*^, Marinez Ferreira de Siqueira ^*1*^
- ^*1*^ Escola Nacional de Botânica Tropical - Jardim Botânico do Rio de Janeiro. Instituto de Pesquisas Jardim Botânico do Rio de Janeiro. Rua Pachecho Leão 2040, Horto, 22040-060 Rio de Janeiro. 
bibliography: bibliography.bib
email: andreasancheztapia@jbrj.gov.br
date: "`r Sys.time()`"
output:
  html_document:
    theme: flatly
    toc: yes
  pdf_document:
    toc: yes
  word_document:
    fig_caption: yes
    keep_md: no
    reference_docx: template_capitulos.docx
csl: journal-of-ecology.csl
abstract: High altitude grasslands in the Brazilian Atlantic Forest possess a variety of species from different biogeographic origins that suggest either a strong connection with the Andes or a connection with other grassland ecosystems in Brazil, such as southern grasslands, adjacent forests, *cerrado* and *campos rupestres*. We aim to (1) understand the assembly of the species pool of one of the core areas for these ecosystems, the Maciço do Caparaó, which has the northernmost position of these ecosystems. We aim to understand which variables discriminate between campos de altitude and other formations, analyze the past connections of the Maciço do Caparaó by searching for analogue climates (no species involved) in the past history of this formation. We chose a climate-only approach because species response is idiosincratic. Climate-only approach may serve as a null hypothesis, where only abiotic, non-interactive, non- idiosyncratic responses may be studied. They assume that climate is more important than the other factors but this may be wrong.
---

# Introduction

+ The current composition of montane communities has legacies resulting from the climatic ang geological history. Other factors (environmental filtering and biotic limitations) operate at smaller scales.  
+ Mountain biodiversity is affected by colonization, in situ speciation, local extinction (Merck et al 2015)
Historical factors affect current composition of communities, because they determine the composition of the species pools that are assembled locally. 
+ In more isolated places, internal speciation is more important than imigration as the mechanism for adding species (Emerson & Gillespie 2008). Isolation either results in higher relative levels of endemism or lower species richness, depending on the dispersal abilities and speciation rates in each present clade.
+ Brazilian highland grasslands are tropical alpine ecosystems in Southeast Brazil, scattered among mountaintops in the Serra do Mar and the Serra da Mantiqueira formations. Three areas encompass most of the campos de altitude, the Itatiaia Plateau, in the Serra da Mantiqueira, the Serra dos Órgãos, in the Serra do Mar and the Caparaó Rangeland, isolated at the northernmost extreme of the Serra da Mantiqueira. 

+ ~~~In addition to relative isolation, another condition for endemism is stability through time (Carnaval et al 2014), that allows the addition of species to the species pools, either trough diversification or my immigration without high rates of extinction.PERO NO VAMOS A HABLAR DE ENDEMISMO!!!~~~



~~~Endemic species may be of any of these origins and therefore represent "centric" or "eccentric" species, sensu Merck et al 2015~~~

Although niche shifts may happen, substantial niche shifts are rare, and the arrival from non-analog areas may be due to local (pre)adapted populations going through the filter at the population level (Merck et al 2015) 

+ Between glaciations, contraction, such as the present. Isolation as a consequence, promoting speciation over migration. During glaciations, expansion and connection with other mountaintops, promoting migration from similar places (1) and connecting with other high altitude floras, such as the Andes. (Safford 2007). Mountaintops have been hypothesized to work as "species pumps" due to these cycles.

**"grasslands were the major biomes in southern brazil during the last glacial period where different forest biomes exist at present Behling 2002 - average annual temperature was about 5-7ºC cooler than today" drier lat pleistocene conditions in SE brazil than in S brazil (Behling 2002)**


+ Grasslands occupied southeast Brazil until the Early Holocene. (Behling 2002)


A hypothetical dispersal route of species are the Andes, going South from Colombia to the Patagonia, and later going Northwards from the southern Brazilian mountains (Serra Geral) up to the Serra do Mar and the Serra da Mantiqueira (Safford 2007). If the dispersal route were found to be plausible, the Caparaó Rangeland would be one of the last high altitude grasslands to receive species from this migration route, due to its northernmost position. It would therefore have less andean species and more relative endemism than other, more southern high-altitude grasslands.

**High levels of endemism in the campos de altitude suggest that climatic isolation has been a constant in these areas, since the maximum pleistocene . The connection between mountaintops in the Pleistocene is frequently suggested and in the case of the campos de altitude, it has been studied using paleobotanical evidence (Behling, leer!) qué hipotesis hay??? south y otras? south, muy páramo, versus otras áreas de campo. se relacionan con las hipotesis de los bosques??**

Ecological niche modeling (ENM) tries to establish a statistical relationship between the known occurrence of a species and the environmental conditions at these points, to predict further areas of potential occurrence in space and time (Giannini et al. 2012). ENM are increasingly being used to answer ecological questions about niche width and conservatism, and more recently, to infer past distributions of species and ecosystems (Hugall et al. 2002, Carnaval & Moritz 2008, Varela et al. 2011).
 "Ecological niche modeling (ENM) is a thriving area with many applications [peterson et al 2011], including recently the exploration of past climatic conditions and their relationship with species distributions. Using different algorithms, ENM techniques try to establish correlative relationships between the environment in which a species can be found and the adequability of the  environmental space. The availability of datasets about past climatic conditions has made it possible to transfer ENM predictions to past conditions.

ENM have been increasingly used to hindcast past distributions of current species, to test for biogeographical hypothesis (Hugall et al. 2002, Carnaval & Moritz 2008, Varela et al. 2011). 

In addition to the uncertainties present in ENM, the use of ENM for extrapolation to past climates has to be done carefully, making some assumptions explicit and when possible, testing these assumptions. Such as niche conservatism. 
GCMs (Varela et al 2015)
Algorithms (Varela et al 2011)
Variables (Varela et al 2015)

In Brazil, paleodistributions have been assessed for the Mata Atlântica (Carnaval), dry forests (Werneck et al 2011, Colevatti et al 2013).cerrado (Werneck et al 2012)
Montane environments have been studied relative to animal clades Batalha Filho et al 2012, Amaro et 2012"

This paper aims to analyze the degree of past isolation of the Caparaó rangeland *relative to other Brazilian mountain ranges, specially the Mantiqueira Range,and other minor elevations in Espirito Santo and Minas Gerais using ENM projections to past environmental conditions.* This will be obtained from the potential distribution of several species currently present in the region's high-altitude grasslands. The potential current distribution is fitted and the better-performing models will be projected to past conditions in several periods of time. This analysis will look for potential dispersal routes from other high-altitude grassland areas (supporting the "andean hypothesis") or past connections to other ecoystems, such as the *campos rupestres*. 
The analysis also aims to understand which clades were able to actively use these connections.

_We aim to determine dispersal routes between the Caparaó and other mountaintops, and between *campos de altitude* in southeast Brazil as a whole and other mounain ranges in South America. We want to test the hypothesis that Caparaó has the highest endemism rates due to a stronger isolation in the past, due to its northernmost position. Alpine species would have a Andes/Campos sulinos/Campos de altitude route, while other species arrived from adjacent forests and lowland grasslands (a "grassland connection" hypothesis).
das quais o Maciço do Caparaó esteve conectado biogeograficamente às demais áreas de campos de altitude, se estas suportam a hipótese de entrada via campos sulinos, e quais clados foram capazes de usar ativamente estas conexões. 
1. Endemic and campos de altitude exclusive species came from the south, through an Andes -> campos sulinos -> campos de altitude connection
2. Widely distributed species came from other grasslands. 
Endemism is stronger in restricted groups and thus has a phylogenetic signal. 
To test the widely accepted hypothesis that the high levels of endemism are the result of historic isolation.
For this, we explore the current and past environmental conditions present in the grassland highlands (**campos de altitude**) at the top of the Caparaó. We do this in relation to the whole environmental space corresponding to these ecosystems, and in relation to the available information about taxa in the region. 
Since biological information about campos de altitude is scarce and may be subject to error, the environment-only approach will be used to contrast and test biological model outputs._

# Material and methods

## Species selection
A preliminary list of species from the campos de altitude of the Caparaó Rangeland was established, based on field surveys and checking names with the Flora of Brazil list (Forzza 2013a, 2013b). The current occurrence records were obtained in the SpeciesLink database.

We classified species in three groups, according to their current distribution and niche width.

1. Species that are endemic to the Caparaó; 
2. species exclusive to the campos de altitude in the Brazilian Atlantic Forest;
3. widely distributed alpine species, with Andean and Euro-alpine distributions. 

We also determined a niche-evolving group of species, with disjunct distribution in other Brazilian lowland grasslands such as the cerrado and the campos rupestres (Alves Y KolbeK). We expect that possible connections between campos de altitude will be highlighted by species in the endemics and exclusive campos group. 



 Species | Family | Current distribution | number of occurrences 
-- | -| ---| -- 
*Baccharis caparoense* | Asteraceae | endemic | d |
*Baccharis platypoda* | Asteraceae | campos de altitude only | d |
 *Achyrocline satureioides* | Asteraceae | widespread alpine | d |
 a | b | widespread grasslands | d |
 *Baccharis dracunculifolia* | | absent from campos | |


(esto está hecho siguiendo a Collevatti et al 2013)

On one hand ENM assume niche conservatism so the current species will be modeled as if their niche never changed. Endemic species are expected to have narrower niches + dispersal constraints or recent time of diversification, widely distributed *(pero hay paleoendémicas y neoendémicas)*

## GCMs and climatic variables

We used the Ecoclimate database (citation) to fit models in the present and to project them to past conditions according to five AOGCM - CCSM4, GISSE2R, IPSL-CM5A, MIROC-ESM and MPI ESM-P. 
We followed Varela et al 2015 recomendation of selecting variables not only by biological criteria but also to exclude statistical correlated variables and variables that vary a lot between GCMs, such as Bio14 and Bio15.
* South America
* resolution? no downscale
+ No correlated, no Bio14, bio15, least number possible to limit overfitting

## Algorithms
No statistical or discriminating algorithms that used pseudoabsences or absences- (Varela et al 2011)
Bioclim, distância ambiental
Maxent for control? -- it is widely used and tends to overfit 

## Fitting the model 
We used a series of R functions (http://github.com/AndreaSanchezTapia/dismo.mod) based on the `dismo` package Tutorial and drawing on the Biomod2 package style of coding presented in their tutorial to fit multispecies models. 

+ métodos de partição dos dados e validação dos modelos (número de partições, proporção da partição de treino sobre a partição de teste, validação cruzada, jacknife ou bootstrap)

Os modelos para o presente para cada espécie serão selecionados com relação a sua capacidade preditiva (omitindo modelos com altas taxas de omissão, por exemplo) e posteriormente projetados para as condições ambientais ocorrentes no último máximo glacial.

Espera-se que as condições passadas tenham sido adequadas para algumas espécies em áreas que não correspondem a campos de altitude no presente. Superpondo modelos individuais para cada espécie, espera-se saber se alguma rota específica é suportada, especialmente a rota de entrada via campos sulinos. 

BAM diagram configuration - 
Tienen las plantas capacidad de dispersión? A esas escalas de tiempo, probablemente sí, o no no habrían llegado ahi

## Environment based modeling

In addition to species modeling, a "pure environmental" modeling was performed, selecting 5000 random points within the *campos de altitude* and using them as "occurrence points" for modeling. This approach has been criticized by some authors because it is based on no real species response but in our case it allows us to estimate pure environmental similarity, both current and past, independent from idiosincratic species responses and without the influence of factors other than climatic variables that limit species distribution, such as dispersal limitation and biotic interactions. This pure environmental model was compared with the individual species models and their superposition.

# Results
# Discussion

# References
CARNAVAL, A. C. & MORITZ, C. 2008. Historical climate modelling predicts patterns of current biodiversity in the Brazilian Atlantic forest. Journal of Biogeography 35:1187–1201.  
FORZZA, R. C. 2013a. Catálogo de plantas e fungos do Brasil - Vol. 1. Editora FIOCRUZ.  
FORZZA, R. C. 2013b. Catálogo de plantas e fungos do Brasil - Vol. 2. JBRJ.  
GIANNINI, T. C., SIQUEIRA, M. F., ACOSTA, A. L., BARRETO, F. C., SARAIVA, A. M. & ALVES-DOS-SANTOS, I. 2012. Current challenges of species distribution predictive modelling. Rodriguésia 63:733–749.  
HIJMANS, R. J., CAMERON, S., PARRA, J., JONES, P., JARVIS, A. & RICHARDSON, K. 2005. WorldClim. Global climate data. Versión 1.4 (release 3).  
HIJMANS, R. J., PHILLIPS, S., LEATHWICK, J. & ELITH, J. 2013. dismo: Species distribution modeling.  
HUGALL, A., MORITZ, C., MOUSSALLI, A. & STANISIC, J. 2002. Reconciling paleodistribution models and comparative  phylogeography in the Wet Tropics rainforest land snail Gnarosophia bellendenkerensis (Brazier 1875). Proceedings of the National Academy of Sciences 99:6112–6117.  
SAFFORD, H. D. 1999. Brazilian Páramos I. An introduction to the physical environment and vegetation of the campos de altitude. Journal of Biogeography 26:693–712.  
SAFFORD, H. D. 2007. Brazilian Páramos IV. Phytogeography of the campos de altitude. Journal of Biogeography 34:1701–1722.  
SCARANO, F. R. 2002. Structure, Function and Floristic Relationships of Plant Communities in Stressful Habitats Marginal to the Brazilian Atlantic Rainforest. Ann Bot 90:517–524.  
VARELA, S., LOBO, J. M. & HORTAL, J. 2011. Using species distribution models in paleobiogeography: A matter of data, predictors and concepts. Palaeogeography, Palaeoclimatology, Palaeoecology 310:451–463.  

======

Areas that act as species refugia during climate oscilations can have higher levels of endemism or behave as "species pumps" (Willis & Whittaker 2000)

Endemics can be centric or eccentric, we dont have a priori hypothesis in this - but to know that we have to know the age of the clades and of the mountains.
species pool: number and identity of species in a region (Graham et al 2014)

A tabela de ocorrências por espécie será utilizada para extrair os valores de variáveis ambientais climáticas e topográficas, disponíveis em Worldclim (Hijmans et al. 2005). Para cada espécie a colinearidade e importância de cada variável para a distribuição conhecida será analisada, a partir de análises de componentes principais, os valores de correlação par a par entre variáveis corrigidos para testes múltiplos. Esta análise permitirá reduzir o número de variáveis para análises posteriores e entender a importância relativa de cada uma para explicar a distribuição geográfica das espécies. 

Modelos de nicho ecológico para o presente serão realizados para cada espécie. Os detalhes específicos da modelagem, como as variáveis ambientais escolhidas, os algoritmos utilizados (Bioclim, MaxEnt, distância ambiental, SVM, GLM, RandomForests, entre outros) e os métodos de partição dos dados e validação dos modelos (número de partições, proporção da partição de treino sobre a partição de teste, validação cruzada, jacknife ou bootstrap) dependem da quantidade final de registros por espécie e irão variar para cada espécie. Em geral os algoritmos de modelagem têm seu desempenho comprometido quando há poucos pontos de ocorrência (menos de 10), mas a maior parte das espécies em áreas tropicais possui menos de 10 registros, o qual limita as opções de análise.  

Os modelos para o presente para cada espécie serão selecionados com relação a sua capacidade preditiva (omitindo modelos com altas taxas de omissão, por exemplo) e posteriormente projetados para as condições ambientais ocorrentes no último máximo glacial (21.000 anos antes do presente, http://pmip2.lsce.ipsl.fr/). Espera-se que as condições passadas tenham sido adequadas para algumas espécies em áreas que não correspondem a campos de altitude no presente. Superpondo modelos individuais para cada espécie, espera-se saber se alguma rota específica é suportada, especialmente a rota de entrada via campos sulinos. 

Por outro lado, a mesma seleção de variáveis e modelagem de nicho ecológico será realizada na escala de habitat, tomando o shapefile correspondente ao Maciço do Caparaó e extraindo aleatoriamente 5000 pontos. Esta análise de hábitat permite ter uma ideia da mudança nas condições ambientais independente da identidade das espécies e de sua resposta individual. Isto permitirá ter um modelo sem a influência de fatores que limitam a distribuição geográfica das espécies, pois além das condições ambientais abióticas, há outros limites para a distribuição das espécies, como a limitação de acesso/dispersão e as interações bióticas, que fazem com que a distribuição observada das espécies seja necessariamente uma sub-amostra do nicho fundamental dela. A modelagem baseada em pontos aleatórios dentro dos campos de altitude do Maciço do Caparaó será comparada aos modelos individuais e ao modelo resultante da superposição destes.
 
```{r,eval = FALSE}
library(dismo)
Baalti <- gbif(genus = "Baccharis",species = "altimontana",geo = T)
Baplat <- gbif(genus = "Baccharis",species = "platypoda",geo = T)
Chpini <- gbif(genus = "Chusquea",species = "pinifolia",geo = T)
Chpini <- gbif(genus = "Chusquea",species = "pinifolia",geo = T)
library(maps)
map("world","Brazil")
points(Baalti$lon, Baalti$lat, pch=19,cex=0.5)
points(Baplat$lon,Baplat$lat,pch=19,cex=0.5,col="red")
    points(Chpini$lon,Chpini$lat,pch=19,cex=0.5,col="blue")
source("./fct/CopyOfdismo_mod.R")
##### Leer los stacks 
gcms <- list.files("./data/env/")
names
gcm <-names[1]

for (gcm in names){
    proj <- stack(list.files(paste0("./data/env/",gcm),pattern=".tif",full.names = T))
                     plot(proj) 
    }


    dismo.mod()    
    library(raster)
extract())
```
